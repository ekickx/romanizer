# main.py
#
# Copyright 2020 Ghani Rafif
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the sale,
# use or other dealings in this Software without prior written
# authorization.

import sys
import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gio

from romanizer.window import RomanizerWindow


class Romanizer(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='com.gitlab.ekickx.romanizer',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

    # Show the default fisrt window
    # More Info: https://wiki.gnome.org/HowDoI/GtkApplication
    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = RomanizerWindow(application=self)
        win.present()

    # Setup app when it first start
    # More Info: https://wiki.gnome.org/HowDoI/GtkApplication
    def do_startup(self):
        Gtk.Application.do_startup(self)

        # Setup actions
        about_action = Gio.SimpleAction.new('about', None)
        about_action.connect('activate', self.on_about)
        self.add_action(about_action)

    def on_about(self, _action, _param):
        builder = Gtk.Builder.new_from_resource(f'/com/gitlab/ekickx/romanizer/ui/about.ui')
        about = builder.get_object('about')
        about.present()


def main(version):
    app = Romanizer()
    return app.run(sys.argv)
