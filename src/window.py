# window.py
#
# Copyright 2020 Ghani Rafif
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the sale,
# use or other dealings in this Software without prior written
# authorization.

from cutlet import Cutlet as Japanese
from korean_romanizer.romanizer import Romanizer as Korean
from gi.repository import Gtk, Gdk


@Gtk.Template(resource_path='/com/gitlab/ekickx/romanizer/ui/window.ui')
class RomanizerWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'RomanizerWindow'

    lang_switcher = Gtk.Template.Child()
    src_text = Gtk.Template.Child()
    roman_text = Gtk.Template.Child()
    clear_btn = Gtk.Template.Child()
    paste_btn = Gtk.Template.Child()
    copy_btn = Gtk.Template.Child()
    romanize_btn = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # Text buffer
        self.src_buffer = self.src_text.get_buffer()
        self.roman_buffer = self.roman_text.get_buffer()

        # Button
        self.clear_btn.connect("clicked", self.clear_text)
        self.paste_btn.connect("clicked", self.paste_text)
        self.romanize_btn.connect("clicked", self.romanize)
        self.copy_btn.connect("clicked", self.copy_text)

        # Clipboard
        self.clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)

        self.setup()

    def setup(self):
        self.setup_headerbar()

    def setup_headerbar(self):
        languages = [
            ['jp', 'Japanese'],
            ['kr', 'Korean']
        ]
        for language in languages:
            self.lang_switcher.append(language[0], language[1])

        self.lang_switcher.set_active(0)

    def romanize_module(self, text):
        lang_id = self.lang_switcher.get_active_id()
        if lang_id == 'jp':
            return str(Japanese().romaji(text))
        elif lang_id == 'kr':
            return str(Korean(text).romanize())

    def clear_text(self, _button):
        self.src_buffer.set_text('')
        self.roman_buffer.set_text('')

    def copy_text(self, button):
        text = self.roman_buffer.get_text(
            self.roman_buffer.get_start_iter(),
            self.roman_buffer.get_end_iter(),
            True
        )
        self.clipboard.set_text(text, -1)

    def paste_text(self, _button):
        text = self.clipboard.wait_for_text()
        # Insert pasted text on the end-line of src_text
        if text is not None:
            end_src_text = self.src_buffer.get_end_iter()
            self.src_buffer.insert(end_src_text, ' ' + text)

    def romanize(self, _button):
        romanized_text = ""
        text = self.src_buffer.get_text(
            self.src_buffer.get_start_iter(),
            self.src_buffer.get_end_iter(),
            True
        )
        # Romanize perline
        for line in text.splitlines():
            if line != text.splitlines()[0]:
                romanized_text += '\n'
            romanized_text += self.romanize_module(line)
        self.roman_buffer.set_text(romanized_text)

